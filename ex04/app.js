var express = require('express');
var fs = require('fs');

var app = express();

// step 1
var hobbies = ["Sleeping", "Reading", "Procrastinating", "Playing Cricket", "Listening to Music", "Swimming"];

// created an empty array for testing
// var hobbies = [];

app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use(express.static(__dirname + "/public"));

app.get('/', function (req, res) {

    // step 3
    var ranValue = Math.floor(Math.random() * hobbies.length);
    console.log(ranValue);

    // step 2
    var ranHobby = hobbies[ranValue];
    console.log(ranHobby);

    // creating a JSON object - step 4
    var data = {
        hobby: ranHobby,

    // giving access to the entire array with key and a value step 5
        hobbiesArray: hobbies,
    }

    res.render('about', data);
});
app.use(function (req, res, next) {
    res.status(404);
    res.render('404');
});
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.render('500');
});

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});
